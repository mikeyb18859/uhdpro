package net.volumetouch.uhdpro;

import com.deepfriedweb.utilities.NoTitleActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.widget.VideoView;

public class MainActivity extends NoTitleActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		VideoView uhdVideoView = (VideoView) findViewById(R.id.uhdVideo);
		
		//String UrlPath="android.resource://"+getPackageName()+"/"+R.raw.foreman_h264;
		
		//uhdVideoView.setVideoURI(Uri.parse(UrlPath));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
